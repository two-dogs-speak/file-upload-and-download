<%--
  Created by IntelliJ IDEA.
  User: 杨同宇
  Date: 2021/10/6
  Time: 20:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>下载文件显示页面</title>
</head>

<body>
<!-- 遍历Map集合 -->
<c:forEach var="map" items="${fileNameMap}">
    ${map.value}<a href="/down.do?filename=${map.key}">下载</a><br/><br/>
</c:forEach>
</body>
</html>
