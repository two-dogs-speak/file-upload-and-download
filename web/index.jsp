<%--
  Created by IntelliJ IDEA.
  User: 杨同宇
  Date: 2021/8/10
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>上传文件页面</title>
  </head>
  <body>
  <%--  通过表单上传文件
        get:上传文件大小有限制
        post:上传文件大小没有限制
  --%>
  <form action="${pageContext.request.contextPath}/upload.do" enctype="multipart/form-data" method="post">
    上传用户：<input type="text" name="username"><br/>
    <input type="file" name="file1" multiple="multiple"><br>
    <input type="submit"> | <input type="reset">
  </form>
  </body>
</html>
